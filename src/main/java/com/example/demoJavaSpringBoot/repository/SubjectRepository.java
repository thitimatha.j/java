package com.example.demoJavaSpringBoot.repository;

import com.example.demoJavaSpringBoot.model.entity.StudentEntity;
import com.example.demoJavaSpringBoot.model.entity.SubjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectRepository extends JpaRepository<SubjectEntity, String > {
}
