package com.example.demoJavaSpringBoot.repository;

import com.example.demoJavaSpringBoot.model.entity.ScoreEntity;
import com.example.demoJavaSpringBoot.model.entity.SubjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScoreRepository extends JpaRepository<ScoreEntity, Integer> {
}
