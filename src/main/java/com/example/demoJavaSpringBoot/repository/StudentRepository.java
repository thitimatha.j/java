package com.example.demoJavaSpringBoot.repository;

import com.example.demoJavaSpringBoot.model.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Integer > {
    Optional<StudentEntity> findByCitizenId(String citizenId);
    List<StudentEntity> findByFirstNameOrLastName(String firstname, String lastname);
}
