package com.example.demoJavaSpringBoot.service;

import com.example.demoJavaSpringBoot.mapper.StudentInfoMapper;
import com.example.demoJavaSpringBoot.model.entity.StudentEntity;
import com.example.demoJavaSpringBoot.model.entity.SubjectEntity;
import com.example.demoJavaSpringBoot.model.request.StudentRequest;
import com.example.demoJavaSpringBoot.model.response.StudentInfo;
import com.example.demoJavaSpringBoot.repository.StudentRepository;
import com.example.demoJavaSpringBoot.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    public List<String> getAllSubject() {
        List<SubjectEntity> subjectEntities = subjectRepository.findAll();
        List<String> subjects = subjectEntities.stream()
                .map(p -> p.getSubjectName())
                .collect(Collectors.toList());

        return subjects;
    }

}
