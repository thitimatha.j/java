package com.example.demoJavaSpringBoot.service;

import com.example.demoJavaSpringBoot.model.entity.ScoreEntity;
import com.example.demoJavaSpringBoot.model.entity.StudentEntity;
import com.example.demoJavaSpringBoot.model.entity.SubjectEntity;
import com.example.demoJavaSpringBoot.model.request.ScoreRequest;
import com.example.demoJavaSpringBoot.model.response.StudentInfo;
import com.example.demoJavaSpringBoot.repository.ScoreRepository;
import com.example.demoJavaSpringBoot.repository.StudentRepository;
import com.example.demoJavaSpringBoot.repository.SubjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ScoreService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private ScoreRepository scoreRepository;


    public String saveScore(ScoreRequest scoreRequest) {
        // search student id
        Optional<StudentEntity> studentEntity = studentRepository.findByCitizenId(scoreRequest.getCitizenId());

        if (studentEntity.isEmpty()){
            return "Not found in tbl_student";
        }

        // calculate score to grade
        String grade = getGradeByScore(scoreRequest.getScore());

        log.info("{}", scoreRequest.getScore().setScale(2));
        // save score
        ScoreEntity scoreEntity = ScoreEntity.builder()
                .studentId(studentEntity.get().getId())
                .subjectId(scoreRequest.getSubjectId())
                .score(scoreRequest.getScore().setScale(2))
                .grade(grade)
                .build();

        scoreRepository.save(scoreEntity);
        return String.format("save score : %s to grade : %s", scoreRequest.getScore(), grade);
    }

    private String getGradeByScore(BigDecimal score) {
        String grade = "F";
        if (score.compareTo(BigDecimal.valueOf(80)) >= 0) {
            grade = "A";
        } else if ((score.compareTo(BigDecimal.valueOf(80)) < 0 ) && ( score.compareTo(BigDecimal.valueOf(60)) >= 0)) {
            grade = "B";
        } else if ((score.compareTo(BigDecimal.valueOf(60)) < 0 ) && ( score.compareTo(BigDecimal.valueOf(50)) >= 0)) {
            grade = "C";
        } else if ((score.compareTo(BigDecimal.valueOf(50)) < 0 ) && ( score.compareTo(BigDecimal.valueOf(40)) >= 0)) {
            grade = "D";
        }
        return grade;
    }

    public List<StudentInfo> getStudentByGrade(String grade) {
        return null;
    }
}
