package com.example.demoJavaSpringBoot.service;

import com.example.demoJavaSpringBoot.mapper.StudentInfoMapper;
import com.example.demoJavaSpringBoot.model.entity.StudentEntity;
import com.example.demoJavaSpringBoot.model.request.StudentRequest;
import com.example.demoJavaSpringBoot.model.response.StudentInfo;
import com.example.demoJavaSpringBoot.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public List<StudentInfo> getAllStudent() {
        List<StudentEntity> studentEntities = studentRepository.findAll();
        List<StudentInfo> students = studentEntities.stream()
                .map(p -> StudentInfoMapper.INSTANCE.from(p))
                .collect(Collectors.toList());

        return students;
    }

    public String registration(StudentRequest studentRequest) {
        Boolean isCitizenIdExist = studentRepository.findByCitizenId(studentRequest.getCitizenId()).isPresent();
        if (isCitizenIdExist){
            return "Fail to save";
        }

        StudentEntity entity = StudentEntity.builder()
                .age(studentRequest.getAge())
                .citizenId(studentRequest.getCitizenId())
                .firstName(studentRequest.getFirstname())
                .lastName(studentRequest.getLastname())
                .build();

        studentRepository.save(entity);
        return "Success to save";
    }

    public StudentInfo getStudentById(int studentId) {
        StudentInfo studentInfo = null;
        Optional<StudentEntity> studentEntity = studentRepository.findById(studentId);
        if (studentEntity.isPresent()){
            studentInfo = StudentInfoMapper.INSTANCE.from(studentEntity.get());
        }
        return studentInfo;
    }

    public List<StudentInfo> getStudentByName(String firstname, String lastname) {
        List<StudentEntity> studentEntities = studentRepository.findByFirstNameOrLastName(firstname, lastname);
        List<StudentInfo> students = studentEntities.stream()
                .map(p -> StudentInfoMapper.INSTANCE.from(p))
                .collect(Collectors.toList());
        return students;
    }
}
