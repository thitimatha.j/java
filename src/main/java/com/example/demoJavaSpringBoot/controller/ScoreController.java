package com.example.demoJavaSpringBoot.controller;

import com.example.demoJavaSpringBoot.model.request.ScoreRequest;
import com.example.demoJavaSpringBoot.model.response.StudentInfo;
import com.example.demoJavaSpringBoot.service.ScoreService;
import com.example.demoJavaSpringBoot.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/score")
public class ScoreController {

    @Autowired
    private ScoreService scoreService;

    @PostMapping(value = "/save")
    public String saveScore(@RequestBody @Validated ScoreRequest scoreRequest){
        return scoreService.saveScore(scoreRequest);
    }

    @GetMapping (value = "/getGrade")
    public List<StudentInfo> getStudentByGrade(@RequestParam String grade){
        return scoreService.getStudentByGrade(grade);
    }

}
