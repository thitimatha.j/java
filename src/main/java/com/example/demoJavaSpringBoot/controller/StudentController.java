package com.example.demoJavaSpringBoot.controller;

import com.example.demoJavaSpringBoot.model.request.StudentRequest;
import com.example.demoJavaSpringBoot.model.response.StudentInfo;
import com.example.demoJavaSpringBoot.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping(value = "/all")
    public List<StudentInfo> getAllStudent(){
        return studentService.getAllStudent();
    }


    @GetMapping(value = "/id/{id}")
    public StudentInfo getStudentById(@PathVariable int id){
        return studentService.getStudentById(id);
    }

    @PostMapping(value = "/search-name")
    public List<StudentInfo> getStudentByName(@RequestParam String firstname, @RequestParam String lastname){
        return studentService.getStudentByName(firstname, lastname);
    }

    @PostMapping(value = "/registration")
    public String registration(@RequestBody @Validated StudentRequest studentRequest){
        return studentService.registration(studentRequest);
    }
}
