package com.example.demoJavaSpringBoot.controller;

import com.example.demoJavaSpringBoot.model.request.StudentRequest;
import com.example.demoJavaSpringBoot.model.response.StudentInfo;
import com.example.demoJavaSpringBoot.service.StudentService;
import com.example.demoJavaSpringBoot.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subject")
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    @GetMapping(value = "/all")
    public List<String> getAllSubject(){
        return subjectService.getAllSubject();
    }

}
