package com.example.demoJavaSpringBoot.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScoreRequest implements Serializable {

    @NotBlank
    private String citizenId;

    @NotBlank
    private String subjectId;

    @NotNull
    private BigDecimal score;

}
