package com.example.demoJavaSpringBoot.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_score")
public class ScoreEntity implements Serializable {

    @Id
    @Column(name = "student_id")
    private Integer studentId;

    @Column(name = "subject_id")
    private String subjectId;

    @Column(name = "score")
    private BigDecimal score;

    @Column(name = "grade")
    private String grade;

}
