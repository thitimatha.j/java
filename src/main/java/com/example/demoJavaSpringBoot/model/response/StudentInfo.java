package com.example.demoJavaSpringBoot.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentInfo implements Serializable {

    private String studentId;
    private String studentName;
    private String studentAge;
}
