package com.example.demoJavaSpringBoot.mapper;


import com.example.demoJavaSpringBoot.model.entity.StudentEntity;
import com.example.demoJavaSpringBoot.model.response.StudentInfo;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Slf4j
@Mapper(builder = @Builder(disableBuilder = true))
public abstract class StudentInfoMapper {

    public static final StudentInfoMapper INSTANCE = Mappers.getMapper(StudentInfoMapper.class);

    @Mapping(target = "studentId", source = "studentEntity.id" )
    @Mapping(target = "studentAge", source = "studentEntity.age" )
    @Mapping(target = "studentName", ignore = true )
    public abstract StudentInfo from(StudentEntity studentEntity);

    @AfterMapping
    public void after(@MappingTarget StudentInfo target, StudentEntity studentEntity){
        String studentName = String.format("%s %s",studentEntity.getFirstName(), studentEntity.getLastName());
        target.setStudentName(studentName);
    }
}
