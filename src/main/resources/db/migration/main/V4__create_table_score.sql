CREATE TABLE tbl_score (
    student_id int not null,
    subject_id varchar(20) not null,
    score decimal(5,2),
    grade character(1),
    PRIMARY KEY (student_id, subject_id)
);