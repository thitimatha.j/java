CREATE TABLE tbl_student (
    id int not null auto_increment,
    first_name varchar(255),
    last_name varchar(255),
    citizen_id varchar(255),
    age varchar(2),
    PRIMARY KEY (id)
);